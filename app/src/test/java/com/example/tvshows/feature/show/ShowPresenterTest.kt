package com.example.tvshows.feature.show

import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class ShowPresenterTest {

    lateinit var testClass: ShowPresenter

    val showView = mockk<ShowContract.View>(relaxed = true)
    val showModel = mockk<ShowContract.Model>(relaxed = true)

    @Before
    fun beforeTests(){
        testClass = ShowPresenter(showView, showModel)
    }

    @Test
    fun `when presenter executes start, then view executes initView`(){
        testClass.start()           //action(when) -> only testClass actions

        verify { showView.initView() }      //result(then) -> mockks
    }

    @Test
    fun `when presenter executes getCast, then model executes getCast`(){
        testClass.getCast("1")

        verify { showModel.getCast("1", any(), any()) }
    }

    @Test
    fun `given cast, when presenter executes getCast, then view executes setCast`(){
        testClass.getCast("1")

        verify { showView.setCast(any()) }
    }

    @After
    fun afterTests(){
    }
}