package com.example.tvshows.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import android.R.attr.spacing
import android.R.attr.top
import android.R.attr.right
import android.R.attr.left
import android.R.attr.bottom


class GridSpaceItemDecoration(
    private val spanCount: Int,
    private val spacing: Int,
    private val includeEdge: Boolean,
    private val includesHeader: Boolean
) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = if (includesHeader) parent.getChildAdapterPosition(view) + 1 else parent.getChildAdapterPosition(view) // item position
        val column = position % spanCount // item column

        if ((includesHeader && position > 1) || !includesHeader)
            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right =
                    spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
    }
}