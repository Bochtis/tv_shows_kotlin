package com.example.tvshows.utils

class Extras {
    companion object {
        const val HEADER_IMG: String = "header_img"
        const val SHOWS_COUNT: String = "shows_count"
        const val SHOW_ID: String = "show_id"
        const val SHOW: String = "show"
        const val SHOWS: String = "shows"
    }
}