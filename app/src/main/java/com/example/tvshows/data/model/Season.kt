package com.example.tvshows.data.model

data class Season(
    val id: Int = 0,
    val image: Image? = null
)