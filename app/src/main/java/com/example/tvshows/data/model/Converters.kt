package com.example.tvshows.data.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {

    @TypeConverter
    fun fromString(value: String): List<String> {
        val listType = object : TypeToken<List<String>>() {

        }.type
        return Gson().fromJson<List<String>>(value, listType)
    }

    @TypeConverter
    fun fromList(list: List<String>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun stringToImage(data: String?): Image {
        if (data == null) {
            return Image()
        }

        val imageType = object : TypeToken<Image>() {

        }.type

        return Gson().fromJson(data, imageType)
    }

    @TypeConverter
    fun imageToString(image: Image): String {
        return Gson().toJson(image)
    }

    @TypeConverter
    fun stringToRating(data: String?): Rating {
        if (data == null) {
            return Rating()
        }

        val ratingType = object : TypeToken<Rating>() {

        }.type

        return Gson().fromJson(data, ratingType)
    }

    @TypeConverter
    fun ratingToString(rating: Rating): String {
        return Gson().toJson(rating)
    }
}