package com.example.tvshows.data.model

data class Character(
    var url: String = "",
    var name: String = "",
    var image: Image? = null
)