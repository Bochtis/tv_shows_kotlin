package com.example.tvshows.data.model

data class Country(
    var name: String = "",
    var code: String = "",
    var timezone: String = ""
)