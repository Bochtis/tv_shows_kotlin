package com.example.tvshows.data.model

import android.os.Parcel
import android.os.Parcelable

data class Rating(var average: Double = 0.0) : Parcelable {


    constructor(parcel: Parcel) : this() {
        average = parcel.readDouble()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(average)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Rating> {
        override fun createFromParcel(parcel: Parcel): Rating {
            return Rating(parcel)
        }

        override fun newArray(size: Int): Array<Rating?> {
            return arrayOfNulls(size)
        }
    }

}