package com.example.tvshows.data.repo

import com.example.tvshows.data.db.MyDatabase
import com.example.tvshows.data.model.Show
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MoviesRepository(val database: MyDatabase) {

    suspend fun getShows(): List<Show>? {
        // coroutine on IO context
        return withContext(Dispatchers.IO) {
            database.myDao().getShows()
        }
    }

    fun updateShows(shows: List<Show>) {
        GlobalScope.launch(Dispatchers.IO) {
            database.myDao().updateShows(shows)
        }
    }

    suspend fun hasShows(timeout: Long): Boolean {
        val currentTimeInMillis = System.currentTimeMillis()
        // coroutine on IO context
        return withContext(Dispatchers.IO) {
            val size = database.myDao().getOutdatedShows(timeout + currentTimeInMillis)?.size
            size!! > 0
        }
    }

    fun insertShows(shows: List<Show>) {
        GlobalScope.launch(Dispatchers.IO) {
            database.myDao().insertShows(shows)
        }
    }

    fun addToFavorites(show: Show) {
        GlobalScope.launch {
            // coroutine on Main
            show.isFavorite = true
            insertFav(show)
        }
    }

    private suspend fun insertFav(show: Show) {
        // coroutine on IO context
        return withContext(Dispatchers.IO) {
            database.myDao().updateFavorite(show)
        }
    }

    fun removeFromFavorites(show: Show) {
        GlobalScope.launch {
            // coroutine on Main
            show.isFavorite = false
            deleteFav(show)
        }
    }

    private suspend fun deleteFav(show: Show) {
        // coroutine on IO context
        return withContext(Dispatchers.IO) {
            database.myDao().updateFavorite(show)
        }
    }

}