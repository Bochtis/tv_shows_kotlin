package com.example.tvshows.data.model

data class Person(
    var url: String = "",
    var name: String = "",
    var country: Country? = null,
    var birthday: String = "",
    var deathday: String = "",
    var image: Image? = null
)