package com.example.tvshows.data.db

import androidx.room.*
import com.example.tvshows.data.model.Show

@Dao
interface MyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertShows(showsList: List<Show>)

    @Update
    fun updateShows(showsList: List<Show>)

    @Update
    fun updateFavorite(show: Show)

    @Query("SELECT * FROM " + DBConstants.TABLE_NAME_SHOW + " WHERE " + DBConstants.SHOW_STATUS + " = true")
    fun getFavorites(): List<Show>?

    @Query("SELECT * FROM " + DBConstants.TABLE_NAME_SHOW)
    fun getShows(): List<Show>?

    @Query("SELECT * FROM " + DBConstants.TABLE_NAME_SHOW + " WHERE " + DBConstants.SHOW_TIMEOUT + " >= :oneDayTimeout")
    fun getOutdatedShows(oneDayTimeout: Long): List<Show>?
}