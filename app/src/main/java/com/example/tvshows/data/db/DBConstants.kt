package com.example.tvshows.data.db

class DBConstants {
    companion object{
        const val DATABASE_NAME: String = "show_db"
        const val TABLE_NAME_SHOW: String = "show_table"
        const val SHOW_ID: String = "show_id"
        const val SHOW_GENRES: String = "show_genres"
        const val SHOW_NAME: String = "show_name"
        const val SHOW_IMAGE: String = "show_image"
        const val SHOW_RATING: String = "show_rating"
        const val SHOW_SUMMARY: String = "show_summary"
        const val SHOW_PREMIERED: String = "show_premiered"
        const val SHOW_STATUS: String = "show_status"
        const val SHOW_TIMEOUT: String = "show_timeout"
    }
}