package com.example.tvshows.data.model

data class Episode(
    val id: Int = 0,
    val url: String? = null,
    val name: String? = null,
    val season: Int = 0,
    val number: Int = 0,
    val airdate: String? = null,
    val airtime: String? = null,
    val airstamp: String? = null,
    val runtime: Int = 0,
    val image: Image? = null,
    val summary: String? = null
)