package com.example.tvshows.data.rest

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ServiceGenerator {

    val API_BASE_URL = "https://api.tvmaze.com/"

    val httpLoggingInterceptor = HttpLoggingInterceptor()
    val httpClient = OkHttpClient.Builder()

    val builder = Retrofit.Builder()

    var retrofit: Retrofit

    init {
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(httpLoggingInterceptor)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
        builder.baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
        retrofit = builder
            .client(httpClient.build())
            .build()
    }

    fun <S> createService(serviceClass: Class<S>): S {
        val service = retrofit.create(serviceClass)
        return service as S
    }
}
