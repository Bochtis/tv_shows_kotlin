package com.example.tvshows.data.rest

import android.content.Context
import com.example.tvshows.data.model.Cast
import com.example.tvshows.data.model.Episode
import com.example.tvshows.data.model.Season
import com.example.tvshows.data.model.Show
import kotlinx.coroutines.*
import org.jetbrains.anko.toast
import retrofit2.HttpException
import kotlin.coroutines.CoroutineContext

class ApiHelper(val context: Context) {

    interface OnFinishedListener<T> {
        fun onSuccess(response: T)
        fun onFailure(error: String?)
    }

    private val apiInterface = ServiceGenerator.createService(Api::class.java)
    private val coroutineContext: CoroutineContext = Job() + Dispatchers.Default
    private val scope = CoroutineScope(coroutineContext)

    fun getShows(onFinishedListener: OnFinishedListener<List<Show>?>) {
        scope.launch {
            val showsRequest = apiInterface.getShows()
            withContext(Dispatchers.Main) {
                try {
                    val showsResponse = showsRequest.await()
                    if (showsResponse.isSuccessful)
                        onFinishedListener.onSuccess(showsResponse.body())
                    else
                        onFinishedListener.onFailure(showsResponse.message())
                } catch (e: HttpException) {
                    context.toast("Exception ${e.message}")
                } catch (e: Throwable) {
                    context.toast("Ooops: Something else went wrong")
                }
            }
        }
    }

    fun getCast(id: String, onFinishedListener: OnFinishedListener<List<Cast>?>) {
        scope.launch {
            val castRequest = apiInterface.getCast(id)
            withContext(Dispatchers.Main) {
                try {
                    val castResponse = castRequest.await()
                    if (castResponse.isSuccessful)
                        onFinishedListener.onSuccess(castResponse.body())
                    else
                        onFinishedListener.onFailure(castResponse.message())
                } catch (e: HttpException) {
                    context.toast("Exception ${e.message}")
                } catch (e: Throwable) {
                    context.toast("Ooops: Something else went wrong")
                }
            }
        }
    }

    fun getSeasons(id: String, onFinishedListener: OnFinishedListener<List<Season>?>) {
        scope.launch {
            val seasonRequest = apiInterface.getSeasons(id)
            withContext(Dispatchers.Main) {
                try {
                    val seasonResponse = seasonRequest.await()
                    if (seasonResponse.isSuccessful)
                        onFinishedListener.onSuccess(seasonResponse.body())
                    else
                        onFinishedListener.onFailure(seasonResponse.message())
                } catch (e: HttpException) {
                    context.toast("Exception ${e.message}")
                } catch (e: Throwable) {
                    context.toast("Ooops: Something else went wrong")
                }
            }
        }
    }

    fun getEpisodes(id: String, onFinishedListener: OnFinishedListener<List<Episode>?>) {
        scope.launch {
            val episodeRequest = apiInterface.getEpisodes(id)
            withContext(Dispatchers.Main) {
                try {
                    val episodeResponse = episodeRequest.await()
                    if (episodeResponse.isSuccessful)
                        onFinishedListener.onSuccess(episodeResponse.body())
                    else
                        onFinishedListener.onFailure(episodeResponse.message())
                } catch (e: HttpException) {
                    context.toast("Exception ${e.message}")
                } catch (e: Throwable) {
                    context.toast("Ooops: Something else went wrong")
                }
            }
        }
    }

    fun cancelAllRequests() = coroutineContext.cancel()

}