package com.example.tvshows.data.prefs

import android.content.Context
import android.content.SharedPreferences

class Prefs(val context: Context) {
    private val PREFS_NAME = "kotlincodes"
    val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun setHeaderImage(KEY_NAME: String, value: String?) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(KEY_NAME, value)
        editor.apply()
    }

    fun getHeaderImage(KEY_NAME: String): String? {
        return sharedPreferences.getString(KEY_NAME, null)
    }

    fun setShowsCount(KEY_NAME: String, value: Int) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putInt(KEY_NAME, value)
        editor.apply()
    }

    fun getShowsCount(KEY_NAME: String): Int {
        return sharedPreferences.getInt(KEY_NAME, 0)
    }
}