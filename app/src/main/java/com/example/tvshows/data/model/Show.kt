package com.example.tvshows.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.*
import com.example.tvshows.data.db.DBConstants

@Entity(tableName = DBConstants.TABLE_NAME_SHOW)
@TypeConverters(Converters::class)
data class Show(
    @PrimaryKey
    @ColumnInfo(name = DBConstants.SHOW_ID)
    var id: Int = 0,
    @ColumnInfo(name = DBConstants.SHOW_NAME)
    var name: String? = "",
    @ColumnInfo(name = DBConstants.SHOW_GENRES)
    var genres: List<String>? = null,
    @ColumnInfo(name = DBConstants.SHOW_IMAGE)
    var image: Image? = null,
    @ColumnInfo(name = DBConstants.SHOW_RATING)
    var rating: Rating? = null,
    @ColumnInfo(name = DBConstants.SHOW_SUMMARY)
    var summary: String? = "",
    @ColumnInfo(name = DBConstants.SHOW_PREMIERED)
    var premiered: String? = "",
    @ColumnInfo(name = DBConstants.SHOW_STATUS)
    var isFavorite: Boolean = false,
    @ColumnInfo(name = DBConstants.SHOW_TIMEOUT)
    var timeout: Long = 0
) : Parcelable {

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        genres = parcel.createStringArrayList()
        rating = parcel.readParcelable(Rating::class.java.classLoader)
        summary = parcel.readString()
        premiered = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeStringList(genres)
        parcel.writeParcelable(rating, flags)
        parcel.writeString(summary)
        parcel.writeString(premiered)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Show> {
        override fun createFromParcel(parcel: Parcel): Show {
            return Show(parcel)
        }

        override fun newArray(size: Int): Array<Show?> {
            return arrayOfNulls(size)
        }
    }

}