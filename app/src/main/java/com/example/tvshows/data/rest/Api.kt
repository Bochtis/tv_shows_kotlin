package com.example.tvshows.data.rest

import com.example.tvshows.data.model.Cast
import com.example.tvshows.data.model.Episode
import com.example.tvshows.data.model.Season
import com.example.tvshows.data.model.Show
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {

    @GET("shows")
    fun getShows(): Deferred<Response<List<Show>>>

    @GET("shows/{id}/cast")
    fun getCast(@Path("id") id: String): Deferred<Response<List<Cast>>>

    @GET("shows/{id}/seasons")
    fun getSeasons(@Path("id") id: String): Deferred<Response<List<Season>>>

    @GET("seasons/{id}/episodes")
    fun getEpisodes(@Path("id") id: String): Deferred<Response<List<Episode>>>
}