package com.example.tvshows.data.model

data class Image(
    var medium: String = "",
    var original: String = ""
)