package com.example.tvshows.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.tvshows.data.model.Show

@Database(entities = [Show::class], version = 2, exportSchema = false)
abstract class MyDatabase : RoomDatabase() {
    abstract fun myDao(): MyDao

    companion object {

        /**
         * The volatile annotation ensures that the value of the database instance
         * is always up to date, and the same for all the execution threads.
         */
        @Volatile
        private var INSTANCE: MyDatabase? = null

        fun getDataBase(context: Context): MyDatabase? {
            if (INSTANCE == null) {
                synchronized(MyDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context, MyDatabase::class.java, DBConstants.DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase() {
            INSTANCE = null
        }
    }
}