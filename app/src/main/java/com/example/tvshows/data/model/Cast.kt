package com.example.tvshows.data.model

data class Cast(
    var person: Person? = null,
    var character: Character? = null
)