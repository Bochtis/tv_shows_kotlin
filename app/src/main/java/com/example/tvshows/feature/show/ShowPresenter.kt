package com.example.tvshows.feature.show

class ShowPresenter(
    val view: ShowContract.View,
    val model: ShowContract.Model
) : ShowContract.Presenter {

    override fun start() {
        view.initView()
    }

    override fun getCast(id: String) {
        model.getCast(id, { cast -> view.setCast(cast) }, { error -> view.showError(error) })
    }

    override fun getSeasons(id: String) {
        model.getSeasons(id, { seasons -> view.setSeasons(seasons) }, { error -> view.showError(error) })
    }

    override fun getEpisodes(id: String) {
        model.getEpisodes(id, { episodes -> view.setEpisodes(episodes) }, { error -> view.showError(error) })
    }

    override fun onSeasonSelected(seasonId: String) {
        getEpisodes(seasonId)
    }
}