package com.example.tvshows.feature.show

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.tvshows.R
import kotlinx.android.synthetic.main.season_spinner_item.view.*

class EpisodeArrayAdapter(context: Context, resource: Int, val seasons: MutableList<String>) :
    ArrayAdapter<String>(context, resource, seasons) {

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent, R.layout.season_spinner_item)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent, R.layout.season_selected_item)
    }

    private fun getCustomView(position: Int, convertView: View?, parent: ViewGroup, layout: Int): View {
        parent.setPadding(0, 0, 0, 0)
        val layout: View = LayoutInflater.from(context).inflate(layout, parent, false)

        layout.season_txt_view.text = seasons[position]

        return layout
    }
}