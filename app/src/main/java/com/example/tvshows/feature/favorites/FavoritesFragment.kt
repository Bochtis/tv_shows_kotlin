package com.example.tvshows.feature.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.VERTICAL
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.*
import com.example.tvshows.R
import com.example.tvshows.data.db.MyDatabase
import com.example.tvshows.data.model.Show
import kotlinx.android.synthetic.main.fragment_favorites.*

class FavoritesFragment : Fragment(), FavoritesContract.View {

    private lateinit var favoritePresenter: FavoritesPresenter
    private lateinit var favShowsRecyclerAdapter: FavoritesRecyclerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_favorites, container, false)
        favoritePresenter = FavoritesPresenter(this, MyDatabase.getDataBase(context!!)!!)
        favoritePresenter.start()
        return view
    }

    override fun initView() {
        favoritePresenter.getFavorites()
    }

    override fun displayData(shows: List<Show>?) {
        if (shows != null && (shows as Collection<Show>).isNotEmpty())
            setupRecycler(shows)
        else
            no_fav_txt_view.visibility = View.VISIBLE
    }

    override fun goToShowDetails(show: Show) {
        findNavController().navigate(
            FavoritesFragmentDirections.actionFavoritesFragmentToShowFragment(show)
        )
    }

    override fun deleteFavorite(position: Int) {
        favShowsRecyclerAdapter.deleteShow(position)
        if (favShowsRecyclerAdapter.itemCount == 0) no_fav_txt_view.visibility = View.VISIBLE
    }

    private fun setupRecycler(shows: List<Show>?) {
        favShowsRecyclerAdapter = FavoritesRecyclerAdapter(context!!, favoritePresenter, shows as MutableList<Show>)
        val itemTouchHelper = ItemTouchHelper(
            SwipeToDeleteCallback(
                favShowsRecyclerAdapter
            )
        )
        favorites_recycler_view.apply {
            setHasFixedSize(true)
            addItemDecoration(DividerItemDecoration(context, VERTICAL))
            layoutManager = LinearLayoutManager(context)
            adapter = favShowsRecyclerAdapter
            itemTouchHelper.attachToRecyclerView(this)
        }
    }
}