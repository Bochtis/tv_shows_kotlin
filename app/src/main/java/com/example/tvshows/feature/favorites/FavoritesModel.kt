package com.example.tvshows.feature.favorites

import com.example.tvshows.data.db.MyDatabase
import com.example.tvshows.data.model.Show
import kotlinx.coroutines.*

class FavoritesModel(val database: MyDatabase) : FavoritesContract.Model {

    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    override fun getFavorites(onShowsReceived: (List<Show>?) -> Unit) {
        scope.launch {
            onShowsReceived(getFavShows())
        }
    }

    override fun deleteFavorite(show: Show) {
        scope.launch {
            deleteFavShow(show)
        }
    }

    private suspend fun getFavShows(): List<Show>? {
        // coroutine on IO context
        return withContext(Dispatchers.IO) {
            val fShows = database.myDao().getFavorites()
            fShows
        }
    }

    private suspend fun deleteFavShow(show: Show) {
        // coroutine on IO context
        withContext(Dispatchers.IO) {
            show.isFavorite = false
            database.myDao().updateFavorite(show)
        }
    }
}