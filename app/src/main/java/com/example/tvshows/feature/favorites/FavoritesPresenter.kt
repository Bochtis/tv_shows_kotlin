package com.example.tvshows.feature.favorites

import com.example.tvshows.data.db.MyDatabase
import com.example.tvshows.data.model.Show

class FavoritesPresenter(val view: FavoritesContract.View, database: MyDatabase) : FavoritesContract.Presenter {

    val model = FavoritesModel(database)

    override fun start() {
        view.initView()
    }

    override fun getFavorites() {
        model.getFavorites { shows -> view.displayData(shows) }
    }

    override fun deleteFavorite(show: Show, position: Int) {
        model.deleteFavorite(show)
        view.deleteFavorite(position)
    }

    override fun onShowClicked(show: Show) {
        view.goToShowDetails(show)
    }
}