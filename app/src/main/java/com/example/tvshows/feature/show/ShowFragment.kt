package com.example.tvshows.feature.show

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tvshows.R
import com.example.tvshows.data.model.Cast
import com.example.tvshows.data.model.Episode
import com.example.tvshows.data.model.Season
import com.example.tvshows.data.model.Show
import com.example.tvshows.data.rest.ApiHelper
import kotlinx.android.synthetic.main.cast_layout.*
import kotlinx.android.synthetic.main.episode_layout.*
import kotlinx.android.synthetic.main.overview_layout.*
import kotlinx.android.synthetic.main.fragment_show.*

class ShowFragment : Fragment(), ShowContract.View {

    lateinit var show: Show

    var seasonIdMap: HashMap<Int, Int> = HashMap()
    private lateinit var seasonsAdapter: EpisodeArrayAdapter

    private var mEpisodes: List<Episode>? = null
    private lateinit var episodeRecyclerAdapter: EpisodeRecyclerAdapter

    private var mCast: List<Cast>? = null
    private lateinit var castRecyclerAdapter: CastRecyclerAdapter

    lateinit var presenter: ShowPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_show, container, false)
        val args = navArgs<ShowFragmentArgs>().value
        show = args.show

        presenter = ShowPresenter(this, ShowModel(ApiHelper(context!!)))

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.start()
    }

    override fun initView() {
        // header image
        val url: String? = show.image?.original
        Glide.with(context!!).load(url?.replace("http", "https")).into(show_img_view)

        // overview data
        title_txt_view.text = show.name
        genres_txt_view.text = show.genres?.joinToString(separator = ", ")
        on_air_date_txt_view.text = show.premiered
        summary_txt_view.text = getString(R.string.summary)
        rating_value_txt_view.text = String.format("%s/10", show.rating?.average.toString())
        summary_value_txt_view.text = HtmlCompat.fromHtml(show.summary!!, HtmlCompat.FROM_HTML_MODE_LEGACY)

        presenter.getCast(show.id.toString())
        presenter.getSeasons(show.id.toString())

    }

    override fun setCast(cast: List<Cast>?) {
        mCast = cast
        setupCastRecycler()
    }

    private fun setupCastRecycler(){
        castRecyclerAdapter = CastRecyclerAdapter(context, mCast)
        cast_recycler.apply {
            // setting up the adapter
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = castRecyclerAdapter
        }
    }

    override fun setSeasons(seasons: List<Season>?) {
        presenter.getEpisodes(seasons!![0].id.toString())

        val seasonsTxtList: ArrayList<String> = ArrayList()
        var index: Int
        for (season: Season in seasons) {
            index = seasons.indexOf(season)
            seasonIdMap[index] = season.id
            seasonsTxtList.add("Season " + (index + 1))
        }

        // setting up the spinner adapter
        seasonsAdapter = EpisodeArrayAdapter(context!!, R.layout.season_spinner_item, seasonsTxtList)
        spinner.adapter = seasonsAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                presenter.onSeasonSelected(seasonIdMap[position].toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    override fun setEpisodes(episodes: List<Episode>?) {
        mEpisodes = episodes
        setupEpisodeRecycler()
    }

    private fun setupEpisodeRecycler() {
        episodeRecyclerAdapter = EpisodeRecyclerAdapter(context!!, mEpisodes)
        episode_recycler.apply {
            // setting up the adapter
            setHasFixedSize(true)
            addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = episodeRecyclerAdapter
        }
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }
}