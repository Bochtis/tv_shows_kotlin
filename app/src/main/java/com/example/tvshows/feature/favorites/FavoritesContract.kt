package com.example.tvshows.feature.favorites

import com.example.tvshows.data.model.Show

interface FavoritesContract {

    interface Model {

        fun getFavorites(onShowsReceived: (List<Show>?) -> Unit)

        fun deleteFavorite(show: Show)

    }

    interface View {

        fun initView()

        fun displayData(shows: List<Show>?)

        fun deleteFavorite(position: Int)

        fun goToShowDetails(show: Show)

    }

    interface Presenter {

        fun start()

        fun getFavorites()

        fun deleteFavorite(show: Show, position: Int)

        fun onShowClicked(show: Show)

    }

}