package com.example.tvshows.feature.favorites

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tvshows.R
import com.example.tvshows.data.model.Show
import kotlinx.android.synthetic.main.fav_show_item.view.*

class FavoritesRecyclerAdapter(val context: Context, val presenter: FavoritesPresenter, val shows: MutableList<Show>?) :
    RecyclerView.Adapter<FavoritesRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fav_show_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = shows?.size ?: 0

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(shows!![position])
    }

    fun deleteItem(position: Int){
        presenter.deleteFavorite(shows!![position], position)
    }

    fun deleteShow(position: Int){
        shows?.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(show: Show) {
            val url: String = show.image!!.original
            Glide.with(context).load(url.replace("http", "https")).into(itemView.show_img_view)
            itemView.view_foreground.setOnClickListener {
                presenter.onShowClicked(show)
            }
            itemView.title_txt_view.text = show.name
            itemView.genres_txt_view.text = show.genres?.joinToString(separator = ", ")
            itemView.rating_value_txt_view.text = show.rating?.average.toString()
        }

    }
}