package com.example.tvshows.feature.show

import com.example.tvshows.data.model.Cast
import com.example.tvshows.data.model.Episode
import com.example.tvshows.data.model.Season
import com.example.tvshows.data.rest.ApiHelper

class ShowModel(private val apiHelper: ApiHelper) : ShowContract.Model {

    override fun getCast(
        id: String,
        onCastReceived: (cast: List<Cast>?) -> Unit,
        onErrorReceived: (error: String?) -> Unit
    ) {
        apiHelper.getCast(id, object : ApiHelper.OnFinishedListener<List<Cast>?> {
            override fun onSuccess(response: List<Cast>?) {
                onCastReceived(response)
            }

            override fun onFailure(error: String?) {
                onErrorReceived(error)
            }
        })
    }

    override fun getSeasons(
        id: String,
        onSeasonsReceived: (seasons: List<Season>?) -> Unit,
        onErrorReceived: (error: String?) -> Unit
    ) {
        apiHelper.getSeasons(id, object : ApiHelper.OnFinishedListener<List<Season>?> {
            override fun onSuccess(response: List<Season>?) {
                onSeasonsReceived(response)
            }

            override fun onFailure(error: String?) {
                onErrorReceived(error)
            }
        })
    }

    override fun getEpisodes(
        id: String,
        onEpisodesReceived: (episodes: List<Episode>?) -> Unit,
        onErrorReceived: (error: String?) -> Unit
    ) {
        apiHelper.getEpisodes(id, object : ApiHelper.OnFinishedListener<List<Episode>?> {
            override fun onSuccess(response: List<Episode>?) {
                onEpisodesReceived(response)
            }

            override fun onFailure(error: String?) {
                onErrorReceived(error)
            }
        })
    }
}