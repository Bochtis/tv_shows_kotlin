package com.example.tvshows.feature.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.tvshows.R
import com.example.tvshows.data.db.MyDatabase
import com.example.tvshows.data.model.Show
import com.example.tvshows.data.prefs.Prefs
import com.example.tvshows.data.rest.ApiHelper
import com.example.tvshows.utils.GridSpaceItemDecoration
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment(), MainContract.View {

    private lateinit var mainPresenter: MainPresenter
    private var mShows: List<Show>? = null
    private lateinit var mainRecyclerAdapter: MainRecyclerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_main, container, false)
        mainPresenter = MainPresenter(this, MyDatabase.getDataBase(context!!)!!, ApiHelper(context!!), Prefs(context!!))
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainPresenter.start()
    }

    override fun showsPorgress() {
        progress_bar_rel.visibility = View.VISIBLE
    }

    override fun initView() {
        mainPresenter.getShows()
    }

    override fun displayData(shows: List<Show>?) {
        mShows = shows
        updateViews()
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }

    override fun hidePorgress() {
        progress_bar_rel.visibility = View.GONE
    }

    override fun updateShow(show: Show, isFavorite: Boolean, position: Int) {
        show.isFavorite = isFavorite
        mainRecyclerAdapter.notifyItemChanged(position)
    }

    override fun goToShowDetails(show: Show) {
        findNavController().navigate(
            MainFragmentDirections.actionMainFragmentToShowFragment(
                show
            )
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        mainPresenter.onDestroy()
    }

    // setting the layout variables and setting up the adapter
    private fun updateViews() {
        setupAdapter()
    }

    // setting up the adapter
    private fun setupAdapter() {
        mainRecyclerAdapter = MainRecyclerAdapter(context, mainPresenter, mShows)
        val gridLayoutManager = GridLayoutManager(context, 2)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (mainRecyclerAdapter.getItemViewType(position)) {
                    MainRecyclerAdapter.TYPE_HEADER -> 2
                    MainRecyclerAdapter.TYPE_ITEM -> 1
                    else -> 1
                }
            }
        }
        shows_recycler.apply {
            setHasFixedSize(true)
            addItemDecoration(GridSpaceItemDecoration(2, 20, false, true))
            layoutManager = gridLayoutManager
            adapter = mainRecyclerAdapter
        }
    }
}