package com.example.tvshows.feature.show

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tvshows.R
import com.example.tvshows.data.model.Cast
import kotlinx.android.synthetic.main.cast_item.view.*

class CastRecyclerAdapter(private val context: Context?, private val cast: List<Cast>?) :
    RecyclerView.Adapter<CastRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cast_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cast?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(context, cast!![position])
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(context: Context?, cast: Cast) {
            val url:String = cast.person!!.image!!.original
            Glide.with(context!!).load(url.replace("http", "https")).into(itemView.person_img_view)
            itemView.person_name_txt_view.text = cast.person!!.name
            itemView.character_name_txt_view.text = cast.character!!.name
        }

    }
}