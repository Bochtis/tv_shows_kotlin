package com.example.tvshows.feature.main

import com.example.tvshows.data.model.Show
import com.example.tvshows.data.prefs.Prefs
import com.example.tvshows.data.repo.MoviesRepository
import com.example.tvshows.data.rest.ApiHelper
import com.example.tvshows.utils.Extras
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit

class MainInteractor(
    private val moviesRepository: MoviesRepository,
    private val apiHelper: ApiHelper,
    val prefs: Prefs
) : MainContract.Interactor {

    override fun getShows(
        onShowsReceived: (shows: List<Show>?) -> Unit,
        onErrorReceived: (error: String?) -> Unit
    ) {
        GlobalScope.launch {
            // 1. check for recently fetched shows
            // 2. request new ones if necessary and save them to db
            refreshShows(onErrorReceived)
            // 3. get the shows from the db
            getShowsFromRepo(onShowsReceived)
        }
    }

    private fun refreshShows(
        onErrorReceived: (error: String?) -> Unit
    ) {
        GlobalScope.launch {
            if (prefs.getShowsCount(Extras.SHOWS_COUNT) == 0 ||
                moviesRepository.hasShows(FRESH_TIMEOUT)
            ) {
                getShowsFromApi(onErrorReceived)
            }
        }
    }

    private fun getShowsFromApi(onErrorReceived: (error: String?) -> Unit) {
        apiHelper.getShows(object : ApiHelper.OnFinishedListener<List<Show>?> {
            override fun onSuccess(response: List<Show>?) {
                response?.let { shows ->
                    shows.forEach { it.timeout = System.currentTimeMillis() }
                    moviesRepository.insertShows(shows)
                    prefs.setShowsCount(Extras.SHOWS_COUNT, shows.size)
                }
            }

            override fun onFailure(error: String?) {
                onErrorReceived.invoke(error)
            }
        })
    }

    private suspend fun getShowsFromRepo(onShowsReceived: (shows: List<Show>?) -> Unit) {
        return withContext(Dispatchers.Main) {
            onShowsReceived.invoke(moviesRepository.getShows())
        }
    }

    override fun addToFavorites(show: Show) {
        moviesRepository.addToFavorites(show)
    }

    override fun removeFromFavorites(show: Show) {
        moviesRepository.removeFromFavorites(show)
    }

    companion object {
        val FRESH_TIMEOUT = TimeUnit.DAYS.toMillis(1)
    }
}