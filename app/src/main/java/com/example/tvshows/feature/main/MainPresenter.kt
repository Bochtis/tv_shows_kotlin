package com.example.tvshows.feature.main

import com.example.tvshows.data.model.Show
import com.example.tvshows.data.db.MyDatabase
import com.example.tvshows.data.prefs.Prefs
import com.example.tvshows.data.repo.MoviesRepository
import com.example.tvshows.data.rest.ApiHelper

class MainPresenter(private var view: MainContract.View?, database: MyDatabase, apiHelper: ApiHelper, prefs: Prefs) :
    MainContract.Presenter {

    private val model: MainInteractor = MainInteractor(MoviesRepository(database), apiHelper, prefs)

    override fun start() {
        view?.showsPorgress()
        view?.initView()
    }

    override fun getShows() {
        model.getShows({ shows ->
            view?.hidePorgress()
            view?.displayData(shows)
        },
            { error -> view?.showError(error) })
    }

    override fun onFavClicked(show: Show, isFavorite: Boolean, position: Int) {
        if (isFavorite) model.removeFromFavorites(show) else model.addToFavorites((show))
        view?.updateShow(show, !isFavorite, position)
    }

    override fun onShowClicked(show: Show) {
        view?.goToShowDetails(show)
    }

    override fun onDestroy() {
        view = null
    }
}