package com.example.tvshows.feature.main

import com.example.tvshows.data.model.Show

interface MainContract {

    interface View {

        fun showsPorgress()

        fun initView()

        fun displayData(shows: List<Show>?)

        fun showError(error: String?)

        fun hidePorgress()

        fun updateShow(show: Show, isFavorite: Boolean, position: Int)

        fun goToShowDetails(show: Show)

    }

    interface Presenter {

        fun start()

        fun getShows()

        fun onFavClicked(show: Show, isFavorite: Boolean, position: Int)

        fun onShowClicked(show: Show)

        fun onDestroy()

    }

    interface Interactor {

        fun getShows(
            onShowsReceived: (shows: List<Show>?) -> Unit,
            onErrorReceived: (error: String?) -> Unit
        )

        fun addToFavorites(show: Show)

        fun removeFromFavorites(show: Show)

    }
}