package com.example.tvshows.feature.show

import com.example.tvshows.data.model.Cast
import com.example.tvshows.data.model.Episode
import com.example.tvshows.data.model.Season

interface ShowContract {

    interface Model {

        fun getCast(
            id: String,
            onCastReceived: (cast: List<Cast>?) -> Unit,
            onErrorReceived: (error: String?) -> Unit
        )

        fun getSeasons(
            id: String,
            onSeasonsReceived: (seasons: List<Season>?) -> Unit,
            onErrorReceived: (error: String?) -> Unit
        )

        fun getEpisodes(
            id: String,
            onEpisodesReceived: (episodes: List<Episode>?) -> Unit,
            onErrorReceived: (error: String?) -> Unit
        )

    }

    interface View {

        fun initView()

        fun setCast(cast: List<Cast>?)

        fun setSeasons(seasons: List<Season>?)

        fun setEpisodes(episodes: List<Episode>?)

        fun showError(error: String?)

    }

    interface Presenter {

        fun start()

        fun getCast(id: String)

        fun getSeasons(id: String)

        fun getEpisodes(id: String)

        fun onSeasonSelected(seasonId: String)

    }

}