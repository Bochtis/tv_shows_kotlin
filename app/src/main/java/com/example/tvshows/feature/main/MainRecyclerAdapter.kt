package com.example.tvshows.feature.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tvshows.R
import com.example.tvshows.data.model.Show
import kotlinx.android.synthetic.main.main_recycler_header.view.*
import kotlinx.android.synthetic.main.show_item.view.*
import kotlin.random.Random


class MainRecyclerAdapter(
    private val context: Context?,
    private val mainPresenter: MainPresenter,
    private val shows: List<Show>?
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_HEADER = 0
        const val TYPE_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_HEADER) {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.main_recycler_header, parent, false)
            return MyHeaderViewHolder(itemView)
        }else if (viewType == TYPE_ITEM){
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.show_item, parent, false)
            return MyViewHolder(itemView)
        }
        throw RuntimeException("No match for $viewType.")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyHeaderViewHolder) {
            holder.bind(shows!![position])
        } else if (holder is MyViewHolder) {
            holder.bind(holder, shows!![position], mainPresenter)
        }
    }

    override fun getItemCount(): Int = shows?.size ?: 0

    override fun getItemViewType(position: Int): Int {
        return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(holder: MyViewHolder, show: Show, mainPresenter: MainPresenter) {
            val url: String = show.image!!.original
            Glide.with(context!!).load(url.replace("http", "https")).into(itemView.show_img_view)
            // check if the current show is favorite
            if (show.isFavorite)
                itemView.fav_img_view.setBackgroundResource(R.drawable.ic_favorite_selected)
            else
                itemView.fav_img_view.setBackgroundResource(R.drawable.ic_favorite_unselected)


            itemView.show_img_view.setOnClickListener {
                mainPresenter.onShowClicked(show)
            }
            itemView.fav_img_view.setOnClickListener {
                if (show.isFavorite) {
                    mainPresenter.onFavClicked(show, true, holder.adapterPosition)
                } else {
                    mainPresenter.onFavClicked(show, false, holder.adapterPosition)
                }
            }
        }

    }

    inner class MyHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(show: Show) {
//            val headerImg = shows!![Random.nextInt(shows.size)].image!!.original
            val headerImg = show.image!!.original
            Glide.with(context!!).load(headerImg.replace("http", "https")).into(itemView.image_view)
        }

    }

}