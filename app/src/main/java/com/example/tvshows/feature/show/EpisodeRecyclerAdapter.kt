package com.example.tvshows.feature.show

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tvshows.R
import com.example.tvshows.data.model.Episode
import kotlinx.android.synthetic.main.episode_item.view.*

class EpisodeRecyclerAdapter(private val context: Context, private val episodes: List<Episode>?) :
    RecyclerView.Adapter<EpisodeRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.episode_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return episodes?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(episodes!![position])
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(episode: Episode) {
            val url = episode.image?.medium
            Glide.with(context).load(url?.replace("http", "https")).into(itemView.episode_img_view)
            itemView.title_txt_view.text = String.format("%s. %s", episode.number, episode.name)
            if (episode.summary != null)
                itemView.description_txt_view.text =
                    HtmlCompat.fromHtml(episode.summary, HtmlCompat.FROM_HTML_MODE_LEGACY)
        }
    }
}
