# TV Shows
An Android Application about the latest tv shows.

Find out the latest shows and get a more detailed picture of them(story, rating, cast, seasons). Choose your favorite shows and save them.

# What is this repository for?
The purpose of this project is to get familiar with the Kotlin Programming language and its practices on some basic components of the Android Jetpack.

# Screenshots

# Libraries used
- [Architecture](https://developer.android.com/topic/libraries/architecture) - A collection of libraries that help you design testable and maintainable apps.
	- [Room](https://developer.android.com/topic/libraries/architecture/room) - Access your app's SQLite database with in-app objects and compile-time checks.
	- [Coroutines](https://developer.android.com/kotlin/coroutines) - A concurrency design pattern that you can use on Android to simplify code that executes asynchronously.
    - [Navigation](https://developer.android.com/guide/navigation) - This refers to the interactions that allow users to navigate across, into, and back out from the different pieces of content within your app.
- [UI](https://developer.android.com/guide/topics/ui) - Details on why and how to use UI Components in your apps - together or separate.
	- [Fragment](https://developer.android.com/guide/components/fragments) - A basic unit of composable UI.
	- [Layout](https://developer.android.com/guide/topics/ui/declaring-layout) - Lay out widgets using different algorithms.
		- [ConstraintLayout](https://developer.android.com/reference/androidx/constraintlayout/widget/ConstraintLayout.html) - A ViewGroup that allows you to position and size widgets in a flexible way.
		- [CardView](https://developer.android.com/reference/androidx/cardview/widget/package-summary.html?hl=en) - A FrameLayout with a rounded corner background and shadow.
        - [ViewPager](https://developer.android.com/reference/android/support/v4/view/ViewPager) - Layout manager that allows the user to flip left and right through pages of data.
- [Material Design](https://www.material.io/develop/android/docs/getting-started/) - An adaptable system of guidelines, components, and tools that support the best practices of user interface design.
- Third party
	- [Retrofit](https://square.github.io/retrofit/) - A library that turns your HTTP API into a Java interface.
    - [OkHttp](https://github.com/square/okhttp) - An HTTP client that’s efficient by default. HTTP is the way modern applications network. It’s how we exchange data & media.
	- [Glide](https://github.com/bumptech/glide) - An image loading and caching library for Android focused on smooth scrolling.
    - [StickyScrollView](https://github.com/didikk/sticky-nestedscrollview) - A library that makes views, inside a scrollview, stick to the top of the scroll container.
    - [SmartTabLayout](https://github.com/ogaclejapan/SmartTabLayout) - A custom ViewPager title strip which gives continuous feedback to the user when scrolling.
    - [Anko](https://github.com/Kotlin/anko) - Anko is a Kotlin library which makes Android application development faster and easier.